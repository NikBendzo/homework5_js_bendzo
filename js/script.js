"use strict";

function createNewUser() {
  let firstName = prompt("enter first name:");
  let secondName = prompt("enter second name");

  while (
    !firstName ||
    !isNaN(firstName) ||
    firstName.trim() === "" ||
    !secondName ||
    !isNaN(secondName) ||
    secondName.trim() === ""
  ) {
    firstName = prompt("enter first name:");
    secondName = prompt("enter second name");
  }

  const newUser = {
    firstname: firstName,
    lastname: secondName,
     setFirstName(data) {
      this.firstname = data;
    },
     setLastName(data) {
      this.lastname = data;
    },

    getLogin() {
      let firstAnswer = this.firstname.charAt(0).toLowerCase();
      let secondAnswer = this.lastname.toLowerCase();
      return `${firstAnswer}${secondAnswer}`;
    },
  };

  return newUser;
}

const result = createNewUser();

console.log(result.getLogin());



// function createNewUser() {
//   const newUser = {};

//   // Перевіряємо ім'я, поки не буде введене вірне значення
//   while (true) {
//     const firstName = prompt('Введіть ваше ім\'я:');

//     if (firstName !== null && firstName.trim() !== "" && !/\d/.test(firstName)) {
//       newUser.firstName = firstName;
//       break; // Виходимо з циклу, якщо отримали правильне значення
//     } else {
//       alert("Будь ласка, введіть вірне ім'я (без цифр)!");
//     }
//   }

//   // Перевіряємо прізвище, поки не буде введене вірне значення
//   while (true) {
//     const lastName = prompt('Введіть ваше прізвище:');

//     if (lastName !== null && lastName.trim() !== "" && !/\d/.test(lastName)) {
//       newUser.lastName = lastName;
//       break; // Виходимо з циклу, якщо отримали правильне значення
//     } else {
//       alert("Будь ласка, введіть вірне прізвище (без цифр)!");
//     }
//   }

//   // Метод для отримання логіну
//   newUser.getLogin = function () {
//     return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
//   };

//   // Функція-сеттер для зміни імені
//   newUser.setFirstName = function (newFirstName) {
//     this.firstName = newFirstName;
//   };

//   // Функція-сеттер для зміни прізвища
//   newUser.setLastName = function (newLastName) {
//     this.lastName = newLastName;
//   };

//   return newUser;
// }

// // Викликати функцію createNewUser() та отримати нового юзера
// const user = createNewUser();

// // Викликати метод getLogin() та вивести результат у консоль
// console.log(user.getLogin());

// // Використовувати функції-сеттери для зміни імені та прізвища
// user.setFirstName('John');
// user.setLastName('Doe');

// // Вивести оновлені дані у консоль
// console.log(user.getLogin());




